package main

import (
	"fmt"
	"math"
)

type punto struct {
	coordX float64
	coordY float64
}

func norm(p punto) float64 {
	return math.Sqrt(math.Pow(p.coordX, 2) + math.Pow(p.coordY, 2))
}

func main() {
	r1, r2, r3 := 107.7, 40.0, 116.619
	p1 := punto{coordX: 0, coordY: 0}
	p2 := punto{coordX: 0, coordY: 100}
	p3 := punto{coordX: 100, coordY: 0}

	p2p1Distance := math.Sqrt(math.Pow(p2.coordX-p1.coordX, 2) + math.Pow(p2.coordY-p1.coordY, 2))
	ex := punto{(p2.coordX - p1.coordX) / p2p1Distance, (p2.coordY - p1.coordY) / p2p1Distance}
	aux := punto{p3.coordX - p1.coordX, p3.coordY - p1.coordY}

	i := ex.coordX*aux.coordX + ex.coordY*aux.coordY
	aux2 := punto{p3.coordX - p1.coordX - i*ex.coordX, p3.coordY - p1.coordY - i*ex.coordY}
	ey := punto{aux2.coordX / norm(aux2), aux2.coordY / norm(aux2)}
	j := ey.coordX*aux.coordX + ey.coordY*aux.coordY

	x := (math.Pow(r1, 2) - math.Pow(r2, 2) + math.Pow(p2p1Distance, 2)) / (2 * p2p1Distance)
	y := (math.Pow(r1, 2)-math.Pow(r3, 2)+math.Pow(i, 2)+math.Pow(j, 2))/(2*j) - i*x/j

	finalX := p1.coordX + x*ex.coordX + y*ey.coordX
	finalY := p1.coordY + x*ex.coordY + y*ey.coordY

	fmt.Println("X:", finalX, " Y:", finalY)
}
